# GIT命令简介

>git在使用的过程中均是使用命令行进行，下面整理的是部分命令。

**1.设置设备的用户名和邮箱，git访问信息**

```shell
git config --global user.name "Your name"
git config --global user.email "email@example.com"
```

**2.创建版本库，命令行进入当前目录**

```shell
git init
```

**3.将文件（text.md）添加到仓库暂存区，文件需放到版本库文件夹内**

```shell
git add text.md
```

错误代码：

当前终端不在仓库目录：``` fatal: not a git repository (or any of the parent directories)```

文件不在仓库内/文件名错误：``` fatal: pathspec 'readme.txt' did not match any files```

**4.将文件提交到仓库当前分支，并备注本次提交的说明（"readme file"）**

```shell
git commit -m "readme file"
```

**5.查看当前仓库内文件信息，是否有已修改文件**

```shell
git status
```

**6.查看文件修改内容**

```shell
git diff readme.ext
```

查看确认无误后，使用```git add``` 、``` git commit -m "massger" ``` 进行提交

**7.查看历史提交记录**

``` shell
git log
git log --pretty=oneline      #简化输出信息
```

查看记录时SHA1值为Git的`commit id` 可以理解为每个版本的版本号

**8.退回历史版本**

```shell
git reset --hard HEAD^      #返回上一个版本
git reset --hard HEAD^^     #返回上上个版本
git reset --hard HEAD~50    #返回往上50个版本
git reset --hard dede035    #返回commit id 开头为dede035的版本
```

**9.查看历史命令**

``` shell 
git reflog
```

> 利用此命令可查询到所有版本的``` commit id```的值

**10.恢复'readme.md'文件为暂存区的版本，如暂存区无版本，恢复为当前分支版本**

``` shell
git restore readme.md
```

**11.清除暂存区'readme.md'文件**

``` shell
git reset HEAD readme.md
```

**12.删除分支'readme.md'文件**

``` shell 
git rm readme.md
git commit -m 'remove readme.md'
```

还可以在工作区删除文件，后使用```git add```、``` git commit```可同样完成此操作

**13.本地库与远程库关联**

可以和gitee和github进行关联，库名称为origin，如想自定义可将其修改为其他

``` shell
git remote add origin git@gitee.com:****/****
```

**14.将本地库内容推送到远程库中**

>本地库master与远程库主分支origin合并

``` shell
git push -u origin master
```

**15.分支创建与切换**

``` shell
git branch aaa    #创建分支aaa
git switch aaa    #切换到分支aaa
git switch -c aaa #创建分支aaa，并切换到aaa分支
git branch        #查看所有分支，*号位置为当前分支
git switch -c aaa origin/aaa	#在本地创建与远程库aaa分支的对应本地分支
```

**16.合并aaa分支**

``` shell
git merge aaa                                  #Fast forward
git merge --no-ff -m "merge with no-ff" aaa    #表示禁用Fast forward
```

fast forward模式删除分支后，会丢掉分支信息，看不出来曾经做过合并。禁用后从分支历史上就可以看出分支信息

**17.查看分支历史**

``` shell
git log --graph --pretty=oneline --abbrev-commit
```

**18.删除aaa分支**

``` shell
git branch -d aaa       #删除已合并完成的分支
git branch -D aaa		#强行删除分支，无论是否已合并完成
```

**19.当前工作区储藏与恢复**

``` shell
git stash           #将当前工作区储藏
git stash list      #查看储藏工作区
git stash apply     #恢复工作区，储藏的工作区不删除
git stash drop      #删除储藏的工作区
git stash pop       #恢复工作区，并删除储藏的工作区
```

**20.将文件``` commit id```修改的内容部分从主分master支修改同步到其他分支aaa上**

``` shell
git cherry-pick commit_id
```

**21.查看远程库信息**

``` shell
git remote		#远程库分支名称
git remote -v	#显示详细信息（抓取和推送地址）
git remote rm origin 	#删除远程库
```

**22.推送分支到远程库**

将分支本地库master分支推送到远程库origin分支上，可根据具体推送接受分支进行更改

``` shell 
git push origin master
```

**23.抓取远程库**

``` shell
git pull
```

**24.建立本地库aaa与远程库分支aaa相连**

```shell
git aaa --set-upstream branch-name origin/aaa
```

**25.整理历史分叉**

``` shell
git rebase
```

把分叉的提交历史“整理”成一条直线，看上去更直观。缺点是本地的分叉提交已经被修改过了。

**26.对当前文档创建删除标签**

``` shell
git tag v1.0		#对当前提交打上标签V1.0
git tag v1.0 commit_id		#对版本号为commit id的提交打上标签v1.0.
git tag -a v0.1 -m "version 0.1 released" commit_id		#用-a指定标签名，-m指定说明文字
git show <tagname>	#查看标签tagname的信息.
git tag -d v0.1		#删除v1.0标签
git push origin v1.0		#推送标签v1.0到远程
git push origin --tags		#推送所有标签到远程
git push origin :refs/tags/v1.0		#删除远程标签v1.0，需先删除本地标签v1.0
```

**27.下载远程库到本地**

``` shell
git clone <远程仓库地址>
```
